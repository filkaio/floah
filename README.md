# Idea behind the project

 is to create easy-to-build, open-source mini-tractors to help with daily garden stuff like scour (weed removing), ploughing, collecting crops etc. 
\
It should be as easy to make as it is possible: for example the frame and wheels could be made out of wood (I have seen wooden wheels in Portugal mounted on normal tractor, and they seemed to work pretty good). Moreover it have to be as ecological as it is possible as well.
\
Obviously it is not new idea to make autonomous helpers.  Few weeks ago  I have found an article desctibing low-cost robot for weed control in fields, which can be found [here](https://arxiv.org/pdf/2112.02162.pdf). Unfortunatly it is not OpenSource, and it is using herbicides, which are not 100% ecological (but there are ecological and easy-to-get alternatives). Nevertheless something like machine described in that work is the clue of this my idea.

# Name

Name comes from **F**ree **L**ibre **O**pen **A**griculture **A**utonomous **H**elper


# Mechanical part

That is the main event for me in this project, as I finish my bachelor in Mechanical Engineering. As I mentioned before, this machine have to be ecological, and easy-to-make from literally scratch, so there is only to ensure it - make it from wood. Of course the main problem of wood is its ability to rapidly oxidize (burn) in case of spark, which can easly happen in such project, as there are many electrical parts. I do not have perfect solution for this problem yet, I have some ideas, like using metal to cover most dangerous parts, or using special type of wood, but it does not solve problem completly. 

The temporary solution is to use metal frame to keep other mechanical and electrical parts together, but it is not easy to make from scratch, and not ecological so it does not fit my requirements.

## Wheels

That is the my job for incoming term - I want to project wooden wheels, which should be available to make without any nails (seems pretty hard to construct). 
First of all I need to find a way how to connect the rims with beams between them.  I have few ideas, but I need to read some more  books about wood technology.


Later, using  an polygon described on circle I need to find optimal geometry. Number of angles should be found by creaing an optimalization algorithm written in Python, and rendered in FreeCAD.  In that work it is necessary to find how different mounting types act under mass of the vehicle and under the stress connected with torque applied after radius change, as the radius of the wheel is not constant, it depends on the position of the wheel: after transition from one angle, which is connected with biggest radius, to another peak of the radius. In this case (as long as I remeber good) normal force is applied to this part of assembly.

To sum up I need to create simulation covering:
* normal force applied after transition between radius peaks
* tangential force applied by mass of the vehicle

To be honest easiest way is to calculate maximum power which will not destroy certain type of wood. Other way is to find engine and calculate its torque, but the concern is how much it is multiplied by differential, as I gonna need to use one. Surerly the next Python script is needed.


# Equipment

## Plants and weed detection
These machines should be equipped with cameras to use DeepWeeds library. Articles about it was written in [Nature](https://www.nature.com/articles/s41598-018-38343-3) and [arXiv](https://arxiv.org/abs/2112.07819).  As you can see the 2nd option offers an idea to make *AutoWeed* - modified quad which uses Arduino for steering and Jetson for weed recognition. 

